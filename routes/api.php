<?php
use Illuminate\Http\Request;

/*
 * |--------------------------------------------------------------------------
 * | API Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register API routes for your application. These
 * | routes are loaded by the RouteServiceProvider within a group which
 * | is assigned the "api" middleware group. Enjoy building your API!
 * |
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    
    Route::group([
        'middleware' => 'auth:api'
    ], function () {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');
        Route::get('clientes', 'ClienteController@index');
        Route::post('clientes', 'ClienteController@store');
        Route::get('clientes/{cliente}', 'ClienteController@show');
        Route::post('clientes/{cliente}', 'ClienteController@update');
        Route::delete('clientes/{cliente}', 'ClienteController@destroy');
        Route::get('productos', 'ProductoController@index');
        Route::post('productos', 'ProductoController@store');
        Route::get('productos/{producto}', 'ProductoController@show');
        Route::post('productos/{producto}', 'ProductoController@update');
        Route::delete('productos/{producto}', 'ProductoController@destroy');
    });
});
