@extends('layouts.app')

@section('datepicker')
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">

  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>
    <!-- Languaje -->
    <script src="{{asset('js/bootstrap-datepicker.es.min.js')}}"></script>
@endsection

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Actualizar un cliente</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('clientes.update', $cliente->id) }}">
            @method('PATCH') 
            @csrf
              <div class="form-group">    
                  <label for="cedula">Cédula:</label>
                  <input type="number" class="form-control" name="cedula" min="0" max="40000000" value="{{ $cliente->cedula }}" />
              </div>
              <div class="form-group">
                  <label for="nombre">Nombre:</label>
                  <input type="text" class="form-control" name="nombre" value="{{ $cliente->nombre }}" />
              </div>
              <div class="form-group">
                  <label for="apellido">Apellido:</label>
                  <input type="text" class="form-control" name="apellido" value="{{ $cliente->apellido }}" />
              </div>
              <div class="form-group">
                  <label for="telefono">Teléfono:</label>
                  <input type="text" class="form-control" name="telefono" value="{{ $cliente->telefono }}" />
              </div>
              <div class="form-group">
                  <label for="email">Correo electrónico:</label>
                  <input type="text" class="form-control" name="email" value="{{ $cliente->email }}" />
              </div>
              <div class="form-group">
                  <label for="fecha_nacimiento">Fecha de nacimiento (DD/MM/AAAA):</label>
                  <input type="text" class="form-control datepicker" name="fecha_nacimiento" value="{{ $cliente->fecha_nacimiento }}" />
              </div>
              <div class="form-group">
                  <label for="estatus">Estatus:</label>
                  <select class="form-control" name="estatus">
                    <option value="activo" {{ ( $cliente->estatus == "activo") ? 'selected' : '' }}>Activo</option>
                    <option value="inactivo" {{ ( $cliente->estatus == "inactivo" ) ? 'selected' : '' }}>Inactivo</option>
                  </select>
              </div>
            <button type="submit" class="btn btn-primary">Actualizar</button>
        </form>
    </div>
</div>
@endsection

@section('script')
<script>
    $('.datepicker').datepicker({  
       format: "dd/mm/yyyy",
       language: "es",
       autoclose: true
     });
</script>
@endsection
