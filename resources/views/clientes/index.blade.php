@extends('layouts.app')

@section('content')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Clientes</h1>    

<div>
    <a style="margin: 19px;" href="{{ route('clientes.create')}}" class="btn btn-primary">Nuevo cliente</a>
    </div>  

  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Cédula</td>
          <td>Nombre y Apellido</td>
          <td>Teléfono</td>
          <td>Correo electrónico</td>
          <td>Fecha de nacimiento</td>
          <td>Estatus</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($clientes as $cliente)
        <tr>
            <td>{{$cliente->id}}</td>
            <td>{{$cliente->cedula}}</td>
            <td>{{$cliente->nombre}} {{$cliente->apellido}}</td>
            <td>{{$cliente->telefono}}</td>
            <td>{{$cliente->email}}</td>
            <td>{{$cliente->fecha_nacimiento}}</td>
            <td>{{$cliente->estatus}}</td>
            <td>
                <a href="{{ route('clientes.edit',$cliente->id)}}" class="btn btn-primary">Modificar</a>
            </td>
            <td>
                <form action="{{ route('clientes.destroy', $cliente->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</div>
@endsection
