@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="content">
                <div class="title m-b-md">
                    Proyecto HKA
                </div>               
                <div class="links">
Desarrollado en <b>Laravel 5.8</b> como prueba de ingreso para el Departamento de Desarrollo Web en <b>The Factory HKA</b><br><b>Ing.</b> Argenis E. Rodríguez R.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
