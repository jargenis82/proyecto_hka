@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Actualizar un producto</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('productos.update', $producto->id) }}" enctype="multipart/form-data">
            @method('PATCH') 
            @csrf
              <div class="form-group">    
                  <label for="cantidad">Cantidad:</label>
                  <input type="number" class="form-control" name="cantidad" value="{{ $producto->cantidad }}" min="0" max="40000000"/>
              </div>
              <div class="form-group">
                  <label for="nombre">Nombre:</label>
                  <input type="text" class="form-control" name="nombre" value="{{ $producto->nombre }}" />
              </div>
              <div class="form-group">
                  <label for="descripcion">Descripción:</label>
                  <input type="text" class="form-control" name="descripcion" value="{{ $producto->descripcion }}" />
              </div>
              <div class="form-group">
                  <label for="precio_unitario">Precio Unitario:</label>
                  <input type="text" class="form-control" name="precio_unitario" value="{{ $producto->precio_unitario }}" />
              </div>
              <div class="form-group">
                  <label for="estatus">Estatus:</label>
                  <select class="form-control" name="estatus">
                    <option value="activo" {{ ( $producto->estatus == "activo") ? 'selected' : '' }}>Activo</option>
                    <option value="inactivo" {{ ( $producto->estatus == "inactivo" ) ? 'selected' : '' }}>Inactivo</option>
                  </select>
              </div>
              <div class="form-group">
                  <label for="ruta_imagen">Imagen:</label>
                  <img src="{{ url('storage/'.$producto->ruta_imagen) }}" alt="" title="" height="100px"/>
                  <input type="file" class="form-control" name="ruta_imagen"/>
              </div>
            <button type="submit" class="btn btn-primary">Actualizar</button>
        </form>
    </div>
</div>
@endsection
