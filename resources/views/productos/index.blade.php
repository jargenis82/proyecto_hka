@extends('layouts.app')

@section('content')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Productos</h1>    

<div>
    <a style="margin: 19px;" href="{{ route('productos.create')}}" class="btn btn-primary">Nuevo producto</a>
    </div>  

  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Cantidad</td>
          <td>Nombre</td>
          <td>Descripción</td>
          <td>Precio Unitario</td>
          <td>Estatus</td>
          <td>Imagen</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($productos as $producto)
        <tr>
            <td>{{$producto->id}}</td>
            <td>{{$producto->cantidad}}</td>
            <td>{{$producto->nombre}}</td>
            <td>{{$producto->descripcion}}</td>
            <td>{{$producto->precio_unitario}}</td>
            <td>{{$producto->estatus}}</td>
            <td><img src="{{ url('storage/'.$producto->ruta_imagen) }}" alt="" title="" height="100px"/>  </td>
            <td>
                <a href="{{ route('productos.edit',$producto->id)}}" class="btn btn-primary">Modificar</a>
            </td>
            <td>
                <form action="{{ route('productos.destroy', $producto->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Eliminar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</div>
@endsection
