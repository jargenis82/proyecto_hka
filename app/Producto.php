<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable = [
        'cantidad',
        'nombre',
        'descripcion',
        'precio_unitario',
        'estatus',
        'ruta_imagen'       
    ];
}
