<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $productos = Producto::all();
        foreach ($productos as $i=>$producto) {
            $producto->estatus = strtoupper($producto->estatus);
            $productos[$i] = $producto;
        }
        
        $ruta = explode("/", $request->path());
        if ($ruta[0] == "api") {
            return response()->json(compact('productos'), 201);
        } else {
            return view('productos.index', compact('productos'));
        }      
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('productos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cantidad'=>'required|numeric',
            'descripcion'=>'between:0,500',
            'nombre'=>'required|between:2,150',
            'precio_unitario'=>'required|numeric',
            'estatus'=>'required'
        ]);
        $path = null;
        if ($request->hasFile('ruta_imagen')) {
            $path = $request->ruta_imagen->store('producto');
        }    
        $producto = new Producto([
            'cantidad' => $request->get('cantidad'),
            'nombre' => $request->get('nombre'),
            'descripcion' => $request->get('descripcion'),
            'precio_unitario' => $request->get('precio_unitario'),
            'estatus' => $request->get('estatus'),
            'ruta_imagen' => $path
        ]);

        $producto->save();
        $ruta = explode("/", $request->path());
        if ($ruta[0] == "api") {
            return response()->json([
                'message' => '¡Producto registrado!'
            ]);
        } else {
            return redirect('/productos')->with('success', '¡Producto registrado!');
        }             
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        //
        $producto = Producto::find($id);
        $ruta = explode("/", $request->path());
        if ($ruta[0] == "api") {
            return response()->json(compact('producto'), 201);
        } else {
            return null;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $producto = Producto::find($id);
        return view('productos.edit', compact('producto'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'cantidad'=>'required|numeric',
            'descripcion'=>'between:0,500',
            'nombre'=>'required|between:2,150',
            'precio_unitario'=>'required|numeric',
            'estatus'=>'required'
        ]);
        $path = null;
        if ($request->hasFile('ruta_imagen')) {
            $path = $request->ruta_imagen->store('producto');
        } 
        $producto = Producto::find($id);
        $producto->cantidad =  $request->get('cantidad');
        $producto->nombre =  $request->get('nombre');
        $producto->descripcion =  $request->get('descripcion');
        $producto->precio_unitario =  $request->get('precio_unitario');
        $producto->estatus =  $request->get('estatus');
        if ($path != null) {
            $producto->ruta_imagen = $path;
        }
        $producto->save();
        $ruta = explode("/", $request->path());
        if ($ruta[0] == "api") {
            return response()->json([
                'message' => '¡Producto actualizado!'
            ]);
        } else {
            return redirect('/productos')->with('success', '¡Producto actualizado!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $producto = Producto::find($id);
        $producto->delete();
        $ruta = explode("/", $request->path());
        if ($ruta[0] == "api") {
            return response()->json([
                'message' => '¡Producto eliminado!'
            ]);
        } else {
            return redirect('/productos')->with('success', '¡Producto eliminado!');
        }
    }
}
