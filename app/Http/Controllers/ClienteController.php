<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;

class ClienteController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $clientes = Cliente::all();
        foreach ($clientes as $i => $cliente) {
            $cliente->estatus = strtoupper($cliente->estatus);
            $cliente->fecha_nacimiento = date('d/m/Y', strtotime($cliente->fecha_nacimiento));
            $clientes[$i] = $cliente;
        }
        
        $ruta = explode("/", $request->path());
        if ($ruta[0] == "api") {
            return response()->json(compact('clientes'), 201);
        } else {
            return view('clientes.index', compact('clientes'));
        }       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'cedula' => 'required|numeric',
            'nombre' => 'required|between:2,20',
            'apellido' => 'required|between:2,20',
            'telefono' => 'required|between:7,50',
            'email' => 'required|email',
            'fecha_nacimiento' => 'required|date_format:"d/m/Y"',
            'estatus' => 'required'
        ]);
        $cliente = new Cliente([
            'cedula' => $request->get('cedula'),
            'nombre' => $request->get('nombre'),
            'apellido' => $request->get('apellido'),
            'telefono' => $request->get('telefono'),
            'email' => $request->get('email'),
            'fecha_nacimiento' => date('Y-m-d', strtotime(str_replace('/', '-', $request->get('fecha_nacimiento')))),
            'estatus' => $request->get('estatus')
        ]);
        $cliente->save();
        $ruta = explode("/", $request->path());
        if ($ruta[0] == "api") {
            return response()->json([
                'message' => '¡Cliente registrado!'
            ]);
        } else {
            return redirect('/clientes')->with('success', '¡Cliente registrado!');
        }        
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
        $cliente = Cliente::find($id);
        $cliente->fecha_nacimiento = date('d/m/Y', strtotime($cliente->fecha_nacimiento));        
        $ruta = explode("/", $request->path());
        if ($ruta[0] == "api") {
            return response()->json(compact('cliente'), 201);
        } else {
            return null;
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::find($id);
        $cliente->fecha_nacimiento = date('d/m/Y', strtotime($cliente->fecha_nacimiento));
        return view('clientes.edit', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'cedula' => 'required|numeric',
            'nombre' => 'required|between:2,20',
            'apellido' => 'required|between:2,20',
            'telefono' => 'required|between:7,50',
            'email' => 'required|email',
            'fecha_nacimiento' => 'required|date_format:"d/m/Y"',
            'estatus' => 'required'
        ]);
        $cliente = Cliente::find($id);
        $cliente->cedula = $request->get('cedula');
        $cliente->nombre = $request->get('nombre');
        $cliente->apellido = $request->get('apellido');
        $cliente->telefono = $request->get('telefono');
        $cliente->email = $request->get('email');
        $cliente->fecha_nacimiento = date('Y-m-d', strtotime(str_replace('/', '-', $request->get('fecha_nacimiento'))));
        $cliente->estatus = $request->get('estatus');
        $cliente->save();
        $ruta = explode("/", $request->path());
        if ($ruta[0] == "api") {
            return response()->json([
                'message' => '¡Cliente actualizado!'
            ]);
        } else {
            return redirect('/clientes')->with('success', '¡Cliente actualizado!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $cliente = Cliente::find($id);
        $cliente->delete();
        $ruta = explode("/", $request->path());
        if ($ruta[0] == "api") {
            return response()->json([
                'message' => '¡Cliente eliminado!'
            ]);
        } else {
            return redirect('/clientes')->with('success', '¡Cliente eliminado!');
        }
        
    }
}
